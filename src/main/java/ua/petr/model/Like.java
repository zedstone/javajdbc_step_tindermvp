package ua.petr.model;

import java.util.Objects;

public class Like {
    private Long whoChose;
    private Long wasChosen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Long id;

    public Like(Long whoChose, Long wasChosen) {
        this.whoChose = whoChose;
        this.wasChosen = wasChosen;
    }
    public Like(Long id, Long whoChose, Long wasChosen) {
        this.whoChose = whoChose;
        this.wasChosen = wasChosen;
    }

    public Long getWhoChose() {
        return whoChose;
    }

    public void setWhoChose(Long whoChose) {
        this.whoChose = whoChose;
    }

    public Long getWasChosen() {
        return wasChosen;
    }

    public void setWasChosen(long wasChosen) {
        this.wasChosen = wasChosen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Like like = (Like) o;
        return whoChose == like.whoChose && wasChosen == like.wasChosen;
    }

    @Override
    public int hashCode() {
        return Objects.hash(whoChose, wasChosen);
    }

    @Override
    public String toString() {
        return "Like{" +
                "whoChose=" + whoChose +
                ", wasChosen=" + wasChosen +
                '}';
    }
}

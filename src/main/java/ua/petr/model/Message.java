package ua.petr.model;

import java.util.Objects;

public class Message {
    private long id;
    private long fromId;
    private long toId;
    private String letter;
    private String messageGroup;

    public Message(long id, long fromId, long toId, String letter) {
        this.id = id;
        this.fromId = fromId;
        this.toId = toId;
        this.letter = letter;
        String min = String.valueOf(Long.min(fromId, toId));
        String max = String.valueOf(Long.max(fromId, toId));
        this.messageGroup = max.concat(min);

    }
    public Message(long fromId, long toId, String letter) {
        this.fromId = fromId;
        this.toId = toId;
        this.letter = letter;
        String min = String.valueOf(Long.min(fromId, toId));
        String max = String.valueOf(Long.max(fromId, toId));
        this.messageGroup = max.concat(min);
    }

    public String getMessageGroup() {
        return messageGroup;
    }

    public void setMessageGroup(String messageGroup) {
        this.messageGroup = messageGroup;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFromId() {
        return fromId;
    }

    public void setFromId(long fromId) {
        this.fromId = fromId;
    }

    public long getToId() {
        return toId;
    }

    public void setToId(long toId) {
        this.toId = toId;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String message) {
        this.letter = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return fromId == message1.fromId && toId == message1.toId && Objects.equals(letter, message1.letter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromId, toId, letter);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", fromId=" + fromId +
                ", toId=" + toId +
                ", message='" + letter + '\'' +
                '}';
    }
}

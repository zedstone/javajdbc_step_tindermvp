package ua.petr.controller;
import ua.petr.model.User;
import ua.petr.services.UserService;


import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;

//@WebServlet(urlPatterns = "/")
public class LoginServlet extends HttpServlet {
    private UserService userService;
    private TemplateEngine templateEngine;

    public LoginServlet(UserService userService, TemplateEngine templateEngine) {
        this.userService = userService;
        this.templateEngine = templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HashMap<String, Object> data = new HashMap<>(1);

        templateEngine.render("login.ftl", data, resp);
        //req.getRequestDispatcher("/hello.ftl").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        User user = userService.findByLoginPass(login, password);

        HashMap<String, Object> data = new HashMap<>(1);
        if(user !=null){
            data.put("users", userService.findAllExceptUser(userService.findByLoginPass(login, password).getId()));

            // templateEngine.render("users.ftl", data, resp);
            resp.sendRedirect("/liked");
        }else {
            templateEngine.render("login.ftl", data, resp);
        }

    }
}

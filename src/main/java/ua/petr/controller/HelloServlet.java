package ua.petr.controller;

import ua.petr.services.LikeService;
import ua.petr.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

//@WebServlet(urlPatterns = "/hello")
public class HelloServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private UserService userService;


    public HelloServlet(UserService userService, TemplateEngine templateEngine) {
        this.userService = userService;
        this.templateEngine = templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                HashMap<String, Object> data = new HashMap<>(1);

                data.put("users", userService.findAll());
                templateEngine.render("users.ftl", data, resp);
                //req.getRequestDispatcher("/hello.ftl").forward(req, resp);
    }
}

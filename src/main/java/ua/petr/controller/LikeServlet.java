package ua.petr.controller;

import ua.petr.model.Like;
import ua.petr.model.User;
import ua.petr.services.LikeService;
import ua.petr.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LikeServlet extends HttpServlet {
    private UserService userService;
    private LikeService likeService;
    private TemplateEngine templateEngine;

    public int endList = 0;
    public Long currentUserid;
    List<User> userList = new ArrayList<>();
    List<User> likedList = new ArrayList<>();

    public LikeServlet(UserService userService, LikeService likeService, TemplateEngine templateEngine) {
        this.userService = userService;
        this.likeService = likeService;
        this.templateEngine = templateEngine;
    }


    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        String idCookie = "id";
        Cookie cookie = null;

        if(cookies !=null) {
            for(Cookie c: cookies) {
                if(idCookie.equals(c.getName())) {
                    cookie = c;
                    break;
                }
            }
        }
        currentUserid = Long.parseLong(cookie.getValue());

        //User user = userService.findByLoginPass(login, password);

        HashMap<String, Object> data = new HashMap<>(1);
        userList = userService.findAllExceptUser(currentUserid);
        data.put("users", userList);
        //data.put("users", userService.findAll());

        if(endList < userList.size()){
            data.put("user", userList.get(endList));
            templateEngine.render("like-page.ftl", data, resp);
        }
        if(endList >= userList.size()){
            data.put("liked_users", likedList);
            System.out.println(likedList);
            templateEngine.render("people-list.ftl", data, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("button-dislike") != null){
            System.out.println("button-dislike");
            ++endList;
            resp.sendRedirect("/liked");
        } else if(req.getParameter("button-like") != null) {
            System.out.println("button-like");
            boolean b = likeService.create(new Like(currentUserid, userList.get(endList).getId()));
            likedList.add(userService.read(userList.get(endList).getId()));
            ++endList;
            resp.sendRedirect("/liked");
        }
    }
}

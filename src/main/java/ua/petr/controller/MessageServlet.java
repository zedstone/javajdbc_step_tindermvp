package ua.petr.controller;

import ua.petr.model.Message;
import ua.petr.model.User;
import ua.petr.services.MessageService;
import ua.petr.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class MessageServlet extends HttpServlet {
    private UserService userService;
    private TemplateEngine templateEngine;
    private MessageService messageService;
    public long currentUserid;
    public long receiveUserId;
    public String messageGroup;
    public MessageServlet(UserService userService, MessageService messageService, TemplateEngine templateEngine) {
        this.userService = userService;
        this.messageService = messageService;
        this.templateEngine = templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        String idCookie = "id";
        Cookie cookie = null;

        if(cookies !=null) {
            for(Cookie c: cookies) {
                if(idCookie.equals(c.getName())) {
                    cookie = c;
                    break;
                }
            }
        }
        currentUserid = Long.parseLong(cookie.getValue());
        String receiveUserIdStr = req.getPathInfo();
        try {
            receiveUserId = Long.parseLong(receiveUserIdStr.substring(1));
        }catch (NumberFormatException e){
            System.out.println(e.getMessage());
            resp.sendRedirect("/liked");
        }
        String min = String.valueOf(Long.min(currentUserid, receiveUserId));
        String max = String.valueOf(Long.max(currentUserid, receiveUserId));
        String messageGroup = max.concat(min);


        HashMap<String, Object> data = new HashMap<>(4);

//        if(messageService.existCorrespondence(messageGroup)){
//            data.put("letters", messageService.readCorrespondence(messageGroup));
//            data.put("user", userService.read(currentUserid));
//            data.put("consumer",userService.read(receiveUserId));
//            data.put("submitMapping",new String("/messages/").concat(String.valueOf(receiveUserId)));
//        }else {
//            messageService.create(new Message(currentUserid, receiveUserId, " "));
//        }
        if(!messageService.existCorrespondence(messageGroup)){
            messageService.create(new Message(currentUserid, receiveUserId, ""));
        }
        data.put("letters", messageService.readCorrespondence(messageGroup));
        data.put("user", userService.read(currentUserid));
        data.put("consumer",userService.read(receiveUserId));
        data.put("submitMapping",new String("/messages/").concat(String.valueOf(receiveUserId)));

        templateEngine.render("chat.ftl", data, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String message = req.getParameter("message");
        messageService.create(new Message(currentUserid, receiveUserId, message));
        System.out.println(new Message(currentUserid, receiveUserId, message));
        resp.sendRedirect(new String("/messages/").concat(String.valueOf(receiveUserId)));
    }
}

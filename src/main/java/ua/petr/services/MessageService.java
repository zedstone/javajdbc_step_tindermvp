package ua.petr.services;

import ua.petr.model.Message;

import java.util.List;

public interface MessageService {
    boolean create(Message message);
    List<Message> readCorrespondence(String messageGroup);

    boolean existCorrespondence(String messageGroup);
}

package ua.petr.services;

import ua.petr.model.Like;

import java.util.List;

public interface LikeService {
    boolean create(Like like);

    List<Like> readFromWho(long fromWho);

    void update(Like like);
    boolean delete(long id);
    List<Like> findAll();
}

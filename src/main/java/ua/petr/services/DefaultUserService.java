package ua.petr.services;

import ua.petr.model.User;
import ua.petr.dao.UserDao;

import java.util.List;

public class DefaultUserService implements UserService{
    private UserDao userDao;

    public DefaultUserService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public boolean create(User user) {
        return userDao.create(user);
    }

    @Override
    public User read(Long id) {
        return userDao.read(id);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public boolean delete(long id) {
        return userDao.delete(id);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public User findByLoginPass(String login, String password) {
        return userDao.findByLoginPass(login, password);
    }

    @Override
    public List<User> findAllExceptUser(Long userId) {
        return userDao.findAllExceptUser(userId);
    }
}

package ua.petr.services;

import ua.petr.model.User;

import java.util.List;

public interface UserService {
    boolean create(User user);
    User read(Long id);
    void update(User user);
    boolean delete(long id);
    List<User> findAll();
    User findByLoginPass(String login,String password);

    List<User> findAllExceptUser(Long userId);
}

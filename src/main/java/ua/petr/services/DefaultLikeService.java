package ua.petr.services;

import ua.petr.dao.LikeDao;
import ua.petr.model.Like;

import java.util.List;

public class DefaultLikeService implements LikeService {
    private LikeDao likeDao;

    public DefaultLikeService(LikeDao likeDao) {
        this.likeDao = likeDao;
    }
    @Override
    public boolean create(Like like) {
        return likeDao.create(like);
    }

    @Override
    public List<Like> readFromWho(long fromWho) {
        return likeDao.readFromWho(fromWho);
    }


    @Override
    public void update(Like like) {
        likeDao.update(like);
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<Like> findAll() {
        return likeDao.findAll();
    }
}

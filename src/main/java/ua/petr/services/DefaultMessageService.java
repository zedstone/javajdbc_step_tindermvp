package ua.petr.services;

import ua.petr.dao.LikeDao;
import ua.petr.dao.MessageDao;
import ua.petr.model.Message;

import java.util.List;

public class DefaultMessageService implements MessageService{
    private MessageDao messageDao;

    public DefaultMessageService(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Override
    public boolean create(Message message) {
        return messageDao.create(message);
    }

    @Override
    public List<Message> readCorrespondence(String messageGroup) {
        return messageDao.readCorrespondence(messageGroup);
    }

    @Override
    public boolean existCorrespondence(String messageGroup) {
        return messageDao.existCorrespondence(messageGroup);
    }
}

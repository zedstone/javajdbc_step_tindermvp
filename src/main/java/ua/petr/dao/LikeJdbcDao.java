package ua.petr.dao;

import org.postgresql.ds.PGPoolingDataSource;
import ua.petr.model.Like;
import ua.petr.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LikeJdbcDao implements LikeDao{

    private PGPoolingDataSource source;

    public LikeJdbcDao() { //my base
        source = new PGPoolingDataSource();
        source.setServerName("ec2-52-49-120-150.eu-west-1.compute.amazonaws.com");
        source.setDatabaseName("d6e1sea259dpch");
        source.setUser("pzykmbqhlfhsad");
        source.setPassword("a9338a572da6c4550ab9908b437be6554bfc6fbf6da3e055c3fc4bcd25b585f9");
        source.setMaxConnections(10);
    }

    @Override
    public boolean create(Like like) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO likes.likes (who_chose, was_chosen) VALUES (?,?)");
            preparedStatement.setLong(1, like.getWhoChose());
            preparedStatement.setLong(2, like.getWasChosen());

            int executionResult = preparedStatement.executeUpdate();
            connection.commit();

            return executionResult > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public List<Like> readFromWho(Long fromWho) {
        List<Like> likes = new ArrayList<>();

        Connection connection = null;
        try {
            connection = source.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM likes.likes WHERE who_chose = ?");
            preparedStatement.setLong(1, fromWho);
                    //("SELECT * FROM likes.likes WHERE who_chose = fromWho");

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                long who_chose = resultSet.getLong(2);
                long was_chosen = resultSet.getLong("was_chosen");
                likes.add(new Like(id, who_chose, was_chosen));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return likes;
    }


    @Override
    public void update(Like like) {

    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<Like> findAll() {
        List<Like> likes = new ArrayList<>();

        Connection connection = null;
        try {
            connection = source.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM likes.likes");

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                long who_chose = resultSet.getLong(2);
                long was_cosen = resultSet.getLong("was_cosen");
                likes.add(new Like(id, who_chose, was_cosen));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return likes;
    }

}


package ua.petr.dao;

//import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import org.postgresql.ds.PGPoolingDataSource;
import ua.petr.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserJdbcDao implements UserDao {
//    private MysqlConnectionPoolDataSource source;
    private PGPoolingDataSource source;

/*    public UserJdbcDao() {
        try {
            source = new MysqlConnectionPoolDataSource();
            source.setServerName("localhost");
            source.setPort(3306);
            source.setDatabaseName("users");
            source.setUser("root");
            source.setPassword("root");
            source.setAllowMultiQueries(true);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }*/

    public UserJdbcDao() { //my base
        source = new PGPoolingDataSource();
        source.setServerName("ec2-52-49-120-150.eu-west-1.compute.amazonaws.com");
        source.setDatabaseName("d6e1sea259dpch");
        source.setUser("pzykmbqhlfhsad");
        source.setPassword("a9338a572da6c4550ab9908b437be6554bfc6fbf6da3e055c3fc4bcd25b585f9");
        source.setMaxConnections(10);
    }
//public UserJdbcDao() { // primer
//    source = new PGPoolingDataSource();
//    source.setServerName("ec2-35-170-239-232.compute-1.amazonaws.com");
//    source.setDatabaseName("demcjnf96hduah");
//    source.setUser("disxdmkwvmnzbs");
//    source.setPassword("9a89674865e3e5c673e5a63128de5bd2b4f5d4653d9a1b22d2694d812202e1ec");
//    source.setMaxConnections(10);
//}

    @Override
    public boolean create(User user) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO users.users VALUES (name = ?, age = ?, login = ?, password = ?, photo =?)");
            //ResultSet resultSet = statement.executeQuery("SELECT * FROM users WHERE id = 3");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setLong(2, user.getAge());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhoto());


            int executionResult = preparedStatement.executeUpdate();
            connection.commit();

            return executionResult > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public User read(Long userId) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users.users WHERE id = ?");
            //ResultSet resultSet = statement.executeQuery("SELECT * FROM users WHERE id = 3");
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString(2);
                int age = resultSet.getInt("age");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String photo = resultSet.getString("photo");
                return new User(id, name, age, login, password, photo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public void update(User user) {

    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        Connection connection = null;
        try {
            connection = source.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users.users");

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString(2);
                int age = resultSet.getInt("age");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String photo = resultSet.getString("photo");
               users.add(new User(id, name, age, login, password, photo));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return users;
    }

    public List<User> findAllExceptUser(Long userId) {
        List<User> users = new ArrayList<>();

        Connection connection = null;
        try {
            connection = source.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users.users");

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString(2);
                int age = resultSet.getInt("age");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String photo = resultSet.getString("photo");
                if (userId != id){
                    users.add(new User(id, name, age, login, password, photo));
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return users;
    }

    @Override
    public User findByLoginPass(String loginUser, String passwordUser) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM users.users WHERE login=? AND password=?");
            //ResultSet resultSet = statement.executeQuery("SELECT * FROM users WHERE id = 3");
            preparedStatement.setString(1, loginUser);
            preparedStatement.setString(2, passwordUser);

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString(2);
                int age = resultSet.getInt("age");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String photo = resultSet.getString("photo");
                return new User(id, name, age, login, password, photo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }
}

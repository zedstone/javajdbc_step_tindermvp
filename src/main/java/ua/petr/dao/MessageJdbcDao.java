package ua.petr.dao;

import org.postgresql.ds.PGPoolingDataSource;
import ua.petr.model.Like;
import ua.petr.model.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageJdbcDao implements MessageDao{

    private PGPoolingDataSource source;

    public MessageJdbcDao() { //my base
        source = new PGPoolingDataSource();
        source.setServerName("ec2-52-49-120-150.eu-west-1.compute.amazonaws.com");
        source.setDatabaseName("d6e1sea259dpch");
        source.setUser("pzykmbqhlfhsad");
        source.setPassword("a9338a572da6c4550ab9908b437be6554bfc6fbf6da3e055c3fc4bcd25b585f9");
        source.setMaxConnections(10);
    }

    @Override
    public boolean create(Message message) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO letters.letters (from_id, to_id, letter, message_group) VALUES (?,?,?,?)");
            preparedStatement.setLong(1, message.getFromId());
            preparedStatement.setLong(2, message.getToId());
            preparedStatement.setString(3, message.getLetter());
            preparedStatement.setString(4, message.getMessageGroup());

            int executionResult = preparedStatement.executeUpdate();
            connection.commit();

            return executionResult > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public boolean existCorrespondence(String messageGroup) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM letters.letters WHERE message_Group = ?");
            preparedStatement.setString(1, messageGroup);

            ResultSet resultSet = preparedStatement.executeQuery();

           while(resultSet.next()) {
               if(resultSet.getLong(1) > 0){
                   return true;
               }
         }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public List<Message> readCorrespondence(String messageGroup) {
        List<Message> messages  = new ArrayList<>();

        Connection connection = null;
        try {
            connection = source.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM letters.letters WHERE message_Group = ?");
            preparedStatement.setString(1, messageGroup);

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {

                long from_id = resultSet.getLong("from_id");
                long to_id = resultSet.getLong("to_id");
                String letter = resultSet.getString("letter");
                messages.add(new Message(from_id, to_id, letter));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return messages;
    }
}

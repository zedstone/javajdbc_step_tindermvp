package ua.petr.dao;


import ua.petr.model.Message;

import java.util.List;

public interface MessageDao {
    boolean create(Message message);
    boolean existCorrespondence(String messageGroup);
    List<Message> readCorrespondence(String messageGroup);
}

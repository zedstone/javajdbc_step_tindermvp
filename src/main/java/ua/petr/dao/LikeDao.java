package ua.petr.dao;

import ua.petr.model.Like;
import ua.petr.model.User;

import java.util.List;

public interface LikeDao {
    boolean create(Like like);
    List<Like> readFromWho(Long fromWho);
    void update(Like like);
    boolean delete(long id);
    List<Like> findAll();

}

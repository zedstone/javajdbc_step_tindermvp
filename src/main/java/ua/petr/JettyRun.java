package ua.petr;


import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ua.petr.controller.*;
import ua.petr.dao.*;
import ua.petr.model.Message;
import ua.petr.services.*;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class JettyRun {
    public static void main(String[] args) throws Exception {
        String portStr = System.getenv("PORT");
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        String username = System.getenv("JDBC_DATABASE_USERNAME");
        String password = System.getenv("JDBC_DATABASE_PASSWORD");
        portStr = portStr == null ? "8080" : portStr;
        Integer port = Integer.parseInt(portStr);
        System.out.println("PORT: " + port);

        Server server = new Server(port);
        ServletContextHandler handler = new ServletContextHandler();
        final UserDao userDao = new UserJdbcDao();
        final LikeDao likeDao = new LikeJdbcDao();
        final MessageDao messageDao = new MessageJdbcDao();


        UserService userService = new DefaultUserService(userDao);
        LikeService likeService = new DefaultLikeService(likeDao);
        MessageService messageService = new DefaultMessageService(messageDao);
        TemplateEngine templateEngine = new TemplateEngine();

        SessionHandler sessionHandler = new SessionHandler();
        handler.setSessionHandler(sessionHandler);


        handler.addServlet(new ServletHolder(new HelloServlet(userService, templateEngine)), "/users");
        handler.addServlet(new ServletHolder(new LoginServlet(userService, templateEngine)), "/");
        handler.addFilter(new FilterHolder(new LoginFilter(templateEngine, userService)), "/*", EnumSet.of(DispatcherType.REQUEST));
        handler.addServlet(new ServletHolder(new FileServlet()), "/assets/*");
        handler.addServlet(new ServletHolder(new LikeServlet(userService,likeService, templateEngine)), "/liked");

        handler.addServlet(new ServletHolder(new MessageServlet(userService, messageService, templateEngine)), "/messages/*");


        server.setHandler(handler);
        server.start();
        server.join();
    }
}

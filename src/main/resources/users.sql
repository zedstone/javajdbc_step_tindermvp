CREATE SCHEMA users;

CREATE TABLE users.users(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40),
    age INTEGER,
    group_Id INTEGER NOT NULL,
    login VARCHAR(50),
    password VARCHAR(50),
    photo VARCHAR(250),
);
INSERT INTO users.users(name, age, login, password, photo) VALUES ('lex', 30, 'lex', 'lex', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9ue5soapGHO0G1hHUcC1ZAvA8E1K-KbLN2A&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('liza', 22, 'liza', 'liza', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSo4ygI8tb200aoY4LQ_ArMM4ZCFE-lynmT-Q&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('amy', 26, 'amy', 'amy', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThHrj3BWE0gAgrFoXhUwbIpmiICPOIs6d4wQ&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('tania', 18, 'tania', 'tania', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOjCWGU_clhTBGqOVUrOCHTxnocBwosFk27g&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('nina', 35, 'nina', 'nina', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQ7Xu70WGsAZ17SdP4M89PbyEuPl2em-1XVQ&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('ira', 41, 'ira', 'ira', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSG83pixWBch-k3av7ULHkIL75mw7eoLa5Kjg&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('adam', 55, 'adam', 'adam', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTORKxjk1_pX-2maZm9VPBmqIbzEAauxms5lA&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('maria', 31, 'maria', 'maria', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuKVqO4Hn_a2ytg7bqUd6ruI68ksegGYiJ4Q&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('olga', 33, 'olga', 'olga', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS12z8smxRr4Aya4Y0QRYdvKYxDQnHVb4zowg&usqp=CAU');
INSERT INTO users.users(name, age, login, password, photo) VALUES ('daniel', 45, 'daniel', 'daniel', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQChx3lhyOTlo0utVmUgFnb04gxJEeTbMvfGA&usqp=CAU');

